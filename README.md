### Details ###

* Course: CIS 3296
* Term: Fall 2018
* Instructor: Michael Wehar
* Teaching Assistant: Rajorshi Biswas

### Getting Started ###

* This repository is for CIS 3296 programming projects.
* Please fork your own copy of this repository and make your copy private.
* Please add dff72324@gmail.com and rajorshi.biswas@temple.edu as collaborators.
