import java.util.Scanner;

public class HumanPlayer extends Player {

	Scanner scanner;
	
	public HumanPlayer(String name) {
		super(null);
		// This constructor should: call the parent class's constructor
		// and initialize scanner
	}

	@Override
	public String makeOneMove() {
		// This method should: prompt for user input of rock, paper, or scissors
		return null;
	}
	
}
