
abstract public class Player {

	String name;
	
	public Player(String name) {
		// This constructor should: initialize name
	}
	
	public String getName() {
		// This method should: return the value of name
		return null;
	}
	
	abstract public String makeOneMove();
	
}
