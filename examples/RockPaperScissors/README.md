### Description ###

A simple RockPaperScissors game written in Java.

### Requirements ###

* It is a two player game

* There are human and computer players

* The game is carried out with a specified number of rounds

* The number of wins for each player is recorded and displayed

### Diagram ###

![Diagram of Classes](diagram.png)

### Specification ###

See the following files for the full specification for this project:

* HumanPlayer.java

* Main.java

* Player.java

* RockPaperScissors.java

* RPSComputerPlayer.java

* ScoreBoard.java

* TwoPlayerGame.java
