
public class RPSComputerPlayer extends Player {

	int rock;
	int paper;
	int scissors;
	
	public RPSComputerPlayer(String name, int rock, int paper, int scissors) {
		super(null);
		// This constructor should: call the parent class's constructor
		// and setup the computer player's logic
	}

	@Override
	public String makeOneMove() {
		// This method should: apply the computer player's logic
		// to make one move
		return null;
	}
	
}
