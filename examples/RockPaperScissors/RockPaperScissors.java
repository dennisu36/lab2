
public class RockPaperScissors extends TwoPlayerGame {

	RockPaperScissors(Player player1, Player player2) {
		// This constructor should: call the parent class constructor
		super(null, null);
	}

	@Override
	public int evaluateMoves(String move1, String move2) {
		// This method should: contain the rock, paper, scissors game logic
		// so that greater than 0 means move1 wins, less than 0 means move2 wins,
		// otherwise neither move wins.
		return 0;
	}
	
}
