import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ScoreBoard {

	Map<String, Integer> scores;
	
	public ScoreBoard(Player player1, Player player2) {
		// This constructor should: initialize scores
		// and add player1 and player2 to the map
	}
	
	public void incrementScore(Player player) {
		// This method should: increment the number of wins for player
	}
	
	public String toString() {
		// This method should: utilize the Arrays class to convert the map data to a string
		return null;
	}
	
}
