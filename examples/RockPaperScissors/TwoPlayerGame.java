
abstract public class TwoPlayerGame {

	ScoreBoard scoreboard;
	Player player1;
	Player player2;
	
	TwoPlayerGame(Player player1, Player player2){
		// This constructor should: initialize scoreboard
		// store references to the inputs player1 and player2
	}
	
	public void playOneRound() {
		// This method should: carry out one round of the game using the evaluateMoves method
	}
	
	public void play(int rounds) {
		// This method should: carry out a specified number of rounds of the game
	}
	
	abstract public int evaluateMoves(String move1, String move2);
	
}
