
public class RPSComputerPlayer extends Player {

	int rock;
	int paper;
	int scissors;
	
	public RPSComputerPlayer(String name, int rock, int paper, int scissors) {
		super(name);
		this.rock = rock;
		this.paper = paper;
		this.scissors = scissors;
	}

	@Override
	public String makeOneMove() {
		int temp = (int)((rock + paper + scissors) * Math.random());
		if(temp < rock) {
			return "rock";
		}
		else if(temp < rock + paper) {
			return "paper";
		}
		else {
			return "scissors";
		}
	}
	
}
