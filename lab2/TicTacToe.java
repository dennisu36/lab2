// Lab Assignment 2: Fill in the code skeleton below.
// You may add additional members variables and methods.
// Please see full instructions: https://templeu.instructure.com/courses/48409/assignments/258398

public class TicTacToe extends TurnBasedGame {

	String move;
	
	int count=0;
	
	char sign1='O';
	char sign2='X';
	char[][] board;
	char playerSwitch;
	
	TicTacToe(Player player1, Player player2) {
		super(player1, player2);
		board = new char[3][3];
		playerSwitch=sign1;
	}

	@Override
	protected void evaluateMove(String move) {

		if(move.equals("0")) {
			if(board[0][0]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				System.out.println("\n");
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[0][0]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("1")) {
			if(board[0][1]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();

				System.out.println("\n");
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[0][1]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("2")) {
			if(board[0][2]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[0][2]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("3")) {
			if(board[1][0]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[1][0]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("4")) {
			if(board[1][1]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[1][1]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("5")) {
			if(board[1][2]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[1][2]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("6")) {
			if(board[2][0]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[2][0]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("7")) {
			if(board[2][1]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[2][1]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else if(move.equals("8")) {
			if(board[2][2]!='*') {
				System.out.println("\nSLOT IS TAKEN\n");

				printBoard();
				playOneTurn();
				finishCurrentTurn();
			}
			else {
				board[2][2]=playerSwitch;
				count++;

				printBoard();
			}
		}
		else {
			System.out.println("\nEnter a valid input(0-8)\n");
			printBoard();
			playOneTurn();
			finishCurrentTurn();
		}
	}

	@Override
	public void play() {
		createboard();
		printBoard();
		System.out.println();
		while(count<9) {
			System.out.println("\nCheckpoint\n");
			if(playerSwitch==sign1) {
				playerSwitch=sign2;
			}
			else {
				playerSwitch=sign1;
			}
			if(count>=5) {
				if(board[0][0]==board[0][1] && board[0][1]==board[0][2] && (board[0][0]=='X' || board[0][0]=='O')) {
					if(board[0][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[1][0]==board[1][1] && board[1][1]==board[1][2] && (board[1][0]=='X' || board[1][0]=='O')) {
					if(board[1][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[2][0]==board[2][1] && board[2][1]==board[2][2] && (board[2][0]=='X' || board[2][0]=='O')) {
					if(board[2][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[0][0]==board[1][0] && board[1][0]==board[2][0] && (board[0][0]=='X' || board[0][0]=='O')) {
					if(board[0][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[0][1]==board[1][1] && board[1][1]==board[2][1] && (board[0][1]=='X' || board[0][1]=='O')) {
					if(board[0][1]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				} 
				else if(board[0][2]==board[1][2] && board[1][2]==board[2][2] && (board[0][2]=='X' || board[0][2]=='O')) {
					if(board[0][2]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[0][0]==board[1][1] && board[1][1]==board[2][2] && (board[0][0]=='X' || board[0][0]=='O')) {
					if(board[0][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else if(board[2][0]==board[1][1] && board[1][1]==board[0][2] && (board[2][0]=='X' || board[2][0]=='O')) {
					if(board[2][0]=='X') {
						System.out.println("Bob won!");
					}
					else {
						System.out.println("Sally won!");
					}
					System.exit(0);
				}
				else {
					
				}
			}
			playOneTurn();
			
		}
		System.out.println("\nIt's a tie game!");
	}
	
	public void createboard() {
		for(int i=0; i<3;i++) {
			for(int j=0;j<3;j++) {
				board[i][j]='*';
			}
		}
	}
	
	public void printBoard(){
		System.out.println("--------------");
		
		for(int i = 0;i<3;i++){
			System.out.print("| ");
			for(int j = 0;j<3;j++){
				System.out.print(board[i][j] + " | ");
			}
			System.out.println();
			System.out.println("---------------");
		}
	}

}
